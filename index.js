const express = require("express")
const app = express()
const PORT = 3000


app.use(express.json())
app.use(express.urlencoded({extended:true}))

app.get("/home", (req, res) => res.status(200).send(`Welcome to the homepage`))

let user = [
    {
        username: "johndoe",
        password: "johndoe1234"
    }
]

app.get("/users", (req, res) => {
    user.push(req.body)
    console.log(user)
})

app.delete("/delete-user", (req, res) => {
    user.pop(req.body)
    console.log(user)
})

app.listen(PORT, () => console.log(`Connected to port ${PORT}`))
